package com.oliveira.giorgy.findbear;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.mbms.StreamingServiceInfo;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

public class FindBear extends AppCompatActivity {
    protected Spinner spn_find_beer_selected;
    protected Button btn_find_beer_start;
    protected TextView tv_find_beer_print;
    protected TextView tv_find_beer_print_brands;
    public BeerExpert  beer_expert= new BeerExpert();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find_bear);

        this.spn_find_beer_selected = findViewById(R.id.spn_find_beer_selected);
        this.btn_find_beer_start = findViewById(R.id.btn_find_beer_start);
        this.tv_find_beer_print = findViewById(R.id.tv_find_beer_print);
        this.tv_find_beer_print = findViewById(R.id.tv_find_beer_print_brands);
    }

    public void onClickFindBeer(View view) {
        String beer_selected = this.spn_find_beer_selected.getSelectedItem().toString();
        this.tv_find_beer_print.setText(beer_selected);

        List<String> beerBrands = this.beer_expert.getBeerBrand(beer_selected);


        StringBuilder string_beer_brands = new StringBuilder();

        for (String brand: beerBrands){
            string_beer_brands.append(brand).append('\n');
        }

        this.tv_find_beer_print_brands.setText(string_beer_brands);

    }
}
