package com.oliveira.giorgy.findbear;

import java.util.ArrayList;
import java.util.List;

public class BeerExpert {

    public List<String> getBeerBrand(String beer_type){
        List<String> beers = new ArrayList<String>();

        switch (beer_type){
            case "Pilsen":
                beers.add("Baden");
                beers.add("Eisenbahn");
                break;

            case "Bock":
                beers.add("Hausen");
                beers.add("Colorado");
                break;
        }

        return beers;
    }
}
